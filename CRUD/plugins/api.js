export default ({ app, store, $axios }) => {

  const processRequest = (promise, resolve, reject) => {

    promise.then(res => {

      if(resolve) resolve(res.data)
      if(reject && res.response.status === 400) reject(res.response.data)

    })

  }

  const api = {

    timeout: 5000,

    get (service, params = {}){
      return new Promise((resolve, reject) => {

        let config = {
          params: params,
          timeout: this.timeout
        }

        processRequest($axios.get("localhost:8000/" + service, config), resolve, reject)

      })
    },

    post(service, params = {}){

      return new Promise((resolve, reject) => {

        let config = {
          timeout: this.timeout
        }

        processRequest($axios.post("localhost:8000/" + service, params, config), resolve, reject)

      })

    }

  }

}

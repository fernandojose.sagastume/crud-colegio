<?php

return [

    'default' => 'crud',
    'migrations' => '__migrations__',
    'connections' => [
        'crud' => array(
            'driver'    => 'mysql',
            'host' => 'localhost',
            'database'  => 'CRUDColegioDB',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci'
        )
    ]

];

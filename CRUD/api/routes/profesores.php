<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' =>'profesores', 'namespace' => 'Solucion'], function () use ($router) {

    $router->get('/', 'ProfesoresController@ObtenerProfesores');
    $router->post('/', 'ProfesoresController@NuevoProfesor');
    $router->post('/{id:[0-9]+}', 'ProfesoresController@ActualizarProfesor');
    $router->post('/eliminar/{id:[0-9]+}', 'ProfesoresController@EliminarProfesor');

});

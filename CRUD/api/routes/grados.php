<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' =>'grados', 'namespace' => 'Solucion'], function () use ($router) {

    $router->get('/', 'GradosController@ObtenerGrados');
    $router->post('/', 'GradosController@NuevoGrado');
    $router->post('/{id:[0-9]+}', 'GradosController@ActualizarGrado');
    $router->post('/eliminar/{id:[0-9]+}', 'GradosController@EliminarGrado');

});

<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' =>'alumnos', 'namespace' => 'Solucion'], function () use ($router) {

    $router->get('/', 'AlumnosController@ObtenerAlumnos');
    $router->post('/', 'AlumnosController@NuevoAlumno');
    $router->post('/{id:[0-9]+}', 'AlumnosController@ActualizarAlumno');
    $router->post('/asignar-grado/{id:[0-9]+}', 'AlumnosController@AsignarGrado');
    $router->post('/desasignar-grado', 'AlumnosController@DesasignarGrado');
    $router->post('/eliminar/{id:[0-9]+}', 'AlumnosController@EliminarAlumno');

});

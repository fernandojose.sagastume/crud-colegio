<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Profesor
 *
 * @property integer idProfesor
 * @property string nombre
 * @property string apellidos
 * @property string genero
 *
 */

class Profesor extends Model {

    protected $table = 'Profesores';
    protected $connection = 'crud';
    protected $primaryKey = 'idProfesor';
    public $timestamps = false;

}

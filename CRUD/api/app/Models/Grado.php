<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Grado
 *
 * @property integer idGrado
 * @property integer idProfesor
 * @property string nombre
 *
 */

class Grado extends Model {

    protected $table = 'Grados';
    protected $connection = 'crud';
    protected $primaryKey = 'idGrado';
    public $timestamps = false;

    public function Profesor(){
        return $this->belongsTo(Profesor::class, "idProfesor", "idProfesor");
    }

}

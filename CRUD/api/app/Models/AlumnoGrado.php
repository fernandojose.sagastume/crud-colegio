<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AlumnoGrado
 *
 * @property integer idAlumno
 * @property integer idGrado
 * @property string seccion
 *
 */

class AlumnoGrado extends Model {

    protected $table = 'AlumnoGrado';
    protected $connection = 'crud';
    protected $primaryKey = ['idAlumno', 'idGrado'];
    public $timestamps = false;
    public $incrementing = false;

    public function Grado(){

        return $this->belongsTo(Grado::class, "idGrado", "idGrado");

    }

}

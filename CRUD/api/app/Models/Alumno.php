<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Alumno
 *
 * @property integer idAlumno
 * @property string nombre
 * @property string apellidos
 * @property string fechaNacimiento
 *
 */

class Alumno extends Model {

    protected $table = 'Alumnos';
    protected $connection = 'crud';
    protected $primaryKey = 'idAlumno';
    public $timestamps = false;

    public function AlumnoGrado(){
        return $this->belongsTo(AlumnoGrado::class, "idAlumno", "idAlumno");
    }

}

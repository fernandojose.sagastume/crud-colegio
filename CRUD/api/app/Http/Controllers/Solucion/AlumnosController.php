<?php

namespace App\Http\Controllers\Solucion;

use App\Http\Controllers\Controller;
use App\Models\Alumno;
use App\Models\AlumnoGrado;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlumnosController extends Controller {

    public function ObtenerAlumnos(Request $request){

        $alumnos = Alumno::with(["AlumnoGrado.Grado"])->get();

        return response()->json($alumnos);

    }

    public function NuevoAlumno(Request $request){

        DB::beginTransaction();

        try{

            $alumno = new Alumno();
            $alumno->nombre = $request->input('nombre', null);
            $alumno->apellidos = $request->input('apellidos', null);
            $alumno->fechaNacimiento = $request->input('fechaNacimiento', null);
            $alumno->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function AsignarGrado(Request $request, $id){

        DB::beginTransaction();

        try{

            $alumnoGrado = new AlumnoGrado();
            $alumnoGrado->idGrado = $request->input('idGrado', null);
            $alumnoGrado->idAlumno = $id;
            $alumnoGrado->seccion = $request->input('seccion', null);
            $alumnoGrado->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function DesasignarGrado(Request $request){

        DB::beginTransaction();

        try{

            AlumnoGrado::with([])
                ->where([
                    ["idGrado", $request->input("idGrado", null)],
                    ["idAlumno", $request->input("idAlumno", null)]
                ])
                ->delete();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function ActualizarAlumno(Request $request, $id){

        DB::beginTransaction();

        try{

            $alumno = Alumno::find($id);
            $alumno->nombre = $request->input('nombre', $alumno->nombre);
            $alumno->apellidos = $request->input('apellidos', $alumno->apellidos);
            $alumno->fechaNacimiento = $request->input('fechaNacimiento', $alumno->fechaNacimiento);
            $alumno->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function EliminarAlumno(Request $request, $id){

        DB::beginTransaction();

        try{

            Alumno::find($id)->delete();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "El usuario no se puede eliminar ya que pertenece a un grado",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

}

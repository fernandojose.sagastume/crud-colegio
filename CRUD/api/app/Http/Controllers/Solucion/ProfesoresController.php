<?php

namespace App\Http\Controllers\Solucion;

use App\Http\Controllers\Controller;
use App\Models\Profesor;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfesoresController extends Controller {

    public function ObtenerProfesores(Request $request){

        $profesores = Profesor::with([])->get();

        return response()->json($profesores);

    }

    public function NuevoProfesor(Request $request){

        DB::beginTransaction();

        try{

            $profesor = new Profesor();
            $profesor->nombre = $request->input('nombre', null);
            $profesor->apellidos = $request->input('apellidos', null);
            $profesor->genero = $request->input('genero', null);
            $profesor->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function ActualizarProfesor(Request $request, $id){

        DB::beginTransaction();

        try{

            $profesor = Profesor::find($id);
            $profesor->nombre = $request->input('nombre', $profesor->nombre);
            $profesor->apellidos = $request->input('apellidos', $profesor->apellidos);
            $profesor->genero = $request->input('genero', $profesor->genero);
            $profesor->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function EliminarProfesor(Request $request, $id){

        DB::beginTransaction();

        try{

            Profesor::find($id)->delete();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "El usuario no se puede eliminar ya que pertenece a un grado",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

}

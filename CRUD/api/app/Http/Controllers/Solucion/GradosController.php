<?php

namespace App\Http\Controllers\Solucion;

use App\Http\Controllers\Controller;
use App\Models\AlumnoGrado;
use App\Models\Grado;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GradosController extends Controller {

    public function ObtenerGrados(Request $request){

        $grados = Grado::with(["Profesor"])->get();

        return response()->json($grados);

    }

    public function NuevoGrado(Request $request){

        DB::beginTransaction();

        try{

            $grado = new Grado();
            $grado->idProfesor = $request->input('idProfesor', null);
            $grado->nombre = $request->input('nombre', null);
            $grado->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function ActualizarGrado(Request $request, $id){

        DB::beginTransaction();

        try{

            $grado = Grado::find($id);
            $grado->nombre = $request->input('nombre', $grado->nombre);
            $grado->idProfesor = $request->input('idProfesor', $grado->idProfesor);
            $grado->save();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

    public function EliminarGrado(Request $request, $id){

        DB::beginTransaction();

        try{

            Grado::find($id)->delete();

            DB::commit();
            return response()->json(null);

        }
        catch(QueryException $e){

            DB::rollBack();
            return response()->json([
                "message" => "El usuario no se puede eliminar ya que el grado tiene alumnos",
                "error" => $e->getMessage()
            ], 400);

        }
        catch(Exception $e){

            DB::rollBack();
            return response()->json([
                "message" => "Ocurrió un error interno en la consulta",
                "error" => $e->getMessage()
            ], 400);

        }

    }

}
